﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLogger
{
    public class FlieSavingThread
    {
        private MainProgram _p;
        private StreamWriter _sw;
        private string _filename;
        private ProfileData[] _pData_buffer = new ProfileData[10];
        //private ProfileData[] _pData_buffer_2 = new ProfileData[10];
        private string[] _gps_info_buffer = new string[10];
        //private string[] _gps_info_buffer_2 = new string[10];
        private string[] _time_buffer = new string[10];
        //private string[] _time_buffer_2 = new string[10];
        public FlieSavingThread (StreamWriter sw, MainProgram p, ProfileData[] pData_buffer, string[] gps_info_buffer, string[] time_fuffer, string filename)
        {
            _sw = sw;
            _p = p;
            _pData_buffer = pData_buffer;
            _gps_info_buffer = gps_info_buffer;
            _time_buffer = time_fuffer;
            _filename = filename;
        }

        public void startSaving()
        {
            bool status = _p.ExportToProfile(_sw, _pData_buffer, _gps_info_buffer, _time_buffer, _filename);
            System.Console.WriteLine("S:" + status);
        }

        
    }
}
