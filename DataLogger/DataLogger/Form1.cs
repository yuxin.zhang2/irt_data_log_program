using SimpleTCP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

// This is the code for your desktop app.
// Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.

namespace DataLogger
{
    public partial class Form1 : Form
    {
        bool isUSB = false;
        bool isLock = false;
        MainProgram p = new MainProgram();
        TCP_Client tcp_client;
        Thread logging_thread;
        System.IO.StreamWriter sw;
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_start_Click(object sender, EventArgs e)
        {
            isLock = true;
            btn_start.Enabled = false;
            string filename = tb_filename.Text+".csv";// Add a Regex for parsing the filename
            string ip_profile_addr = tb_profile_ip.Text;
            string ip_profile_port = tb_profile_port.Text;
            Match match_profile_ip = Regex.Match(ip_profile_addr, @"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}");
            Match match_profile_port = Regex.Match(ip_profile_port, @"\d{1,8}");
            string ip_addr = tb_ip.Text;
            string ip_port = tb_port.Text;
            Match match_ip = Regex.Match(ip_addr, @"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}");
            Match match_port = Regex.Match(ip_port, @"\d{1,8}");
            if (match_ip.Success && match_port.Success)
            {
                int port = int.Parse(match_port.Value);
                tcp_client = new TCP_Client(match_ip.Value, port);
                //Test
                Encoding unicode = System.Text.Encoding.GetEncoding("utf-8");
                sw = new System.IO.StreamWriter(filename, append: true, encoding: unicode);
                sw.WriteLine("Data Logging Starts:" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.FFF"));
                //Test
                p.Initialize();
                if (isUSB)
                {
                    MessageBox.Show("USB_Variables:" + match_ip.Value + " and " + match_port.Value);
                    p.UsbOpen(); //Exception Handler is missing 
                }
                else
                {
                    if (match_profile_ip.Success && match_profile_port.Success)
                    {
                        MessageBox.Show("Ethernet_Variables:" + match_ip.Value + " and " + match_port.Value);
                        p.EthernetOpen(match_profile_ip.Value, match_profile_port.Value); //Exception Handler is missing
                    }
                    else
                    {
                        MessageBox.Show("Profile IP Setting is Wrong! Please check !");
                    }
                }
                // Data Processing
                System.Console.WriteLine("FileName:"+ filename);
                LoggingThread lt = new LoggingThread(sw, tcp_client, p, filename);
                logging_thread = new Thread(new ThreadStart(lt.startLogging));
                logging_thread.Start();
                
            }
            else
            {
                MessageBox.Show("Wrong inputs! Please check !");
            }
        }

        private void rb_usb_CheckedChanged(object sender, EventArgs e)
        {
            if (rb_usb.Checked)
            {
                rb_ethernet.Checked = false;
                isUSB = true;
            }
        }

        private void rb_ethernet_CheckedChanged(object sender, EventArgs e)
        {
            if (rb_ethernet.Checked)
            {
                rb_usb.Checked = false;
                isUSB = false;
            }
        }

        private void btn_stop_Click(object sender, EventArgs e)
        {
            if (isLock)
            {
                sw.Close();
                p.CommClose();
                logging_thread.Abort();
                logging_thread.Join();
                tcp_client.EndConn();
                btn_start.Enabled = true;
                System.Console.WriteLine("Closed !");
            }            
        }
    }
}
