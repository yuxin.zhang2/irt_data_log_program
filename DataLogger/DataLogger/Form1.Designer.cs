namespace DataLogger
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_start = new System.Windows.Forms.Button();
            this.lbl_title = new System.Windows.Forms.Label();
            this.btn_stop = new System.Windows.Forms.Button();
            this.tb_ip = new System.Windows.Forms.TextBox();
            this.lbl_Address = new System.Windows.Forms.Label();
            this.tb_port = new System.Windows.Forms.TextBox();
            this.lbl_port = new System.Windows.Forms.Label();
            this.gb_connection = new System.Windows.Forms.GroupBox();
            this.rb_ethernet = new System.Windows.Forms.RadioButton();
            this.rb_usb = new System.Windows.Forms.RadioButton();
            this.tb_filename = new System.Windows.Forms.TextBox();
            this.lb_filename = new System.Windows.Forms.Label();
            this.gb_gps = new System.Windows.Forms.GroupBox();
            this.gb_profile = new System.Windows.Forms.GroupBox();
            this.lb_profile_port = new System.Windows.Forms.Label();
            this.tb_profile_port = new System.Windows.Forms.TextBox();
            this.lb_profile_ip = new System.Windows.Forms.Label();
            this.tb_profile_ip = new System.Windows.Forms.TextBox();
            this.gb_connection.SuspendLayout();
            this.gb_gps.SuspendLayout();
            this.gb_profile.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_start
            // 
            this.btn_start.Location = new System.Drawing.Point(115, 403);
            this.btn_start.Margin = new System.Windows.Forms.Padding(2);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(97, 28);
            this.btn_start.TabIndex = 2;
            this.btn_start.Text = "Start";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title.Location = new System.Drawing.Point(202, 19);
            this.lbl_title.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(141, 26);
            this.lbl_title.TabIndex = 3;
            this.lbl_title.Text = "Data Logging";
            // 
            // btn_stop
            // 
            this.btn_stop.Location = new System.Drawing.Point(325, 403);
            this.btn_stop.Margin = new System.Windows.Forms.Padding(2);
            this.btn_stop.Name = "btn_stop";
            this.btn_stop.Size = new System.Drawing.Size(97, 28);
            this.btn_stop.TabIndex = 4;
            this.btn_stop.Text = "Stop";
            this.btn_stop.UseVisualStyleBackColor = true;
            this.btn_stop.Click += new System.EventHandler(this.btn_stop_Click);
            // 
            // tb_ip
            // 
            this.tb_ip.Location = new System.Drawing.Point(46, 31);
            this.tb_ip.Name = "tb_ip";
            this.tb_ip.Size = new System.Drawing.Size(151, 20);
            this.tb_ip.TabIndex = 5;
            this.tb_ip.Text = "192.168.28.100";
            // 
            // lbl_Address
            // 
            this.lbl_Address.AutoSize = true;
            this.lbl_Address.Location = new System.Drawing.Point(207, 34);
            this.lbl_Address.Name = "lbl_Address";
            this.lbl_Address.Size = new System.Drawing.Size(72, 13);
            this.lbl_Address.TabIndex = 6;
            this.lbl_Address.Text = "IP ADDRESS";
            // 
            // tb_port
            // 
            this.tb_port.Location = new System.Drawing.Point(46, 67);
            this.tb_port.Name = "tb_port";
            this.tb_port.Size = new System.Drawing.Size(151, 20);
            this.tb_port.TabIndex = 7;
            this.tb_port.Text = "5017";
            // 
            // lbl_port
            // 
            this.lbl_port.AutoSize = true;
            this.lbl_port.Location = new System.Drawing.Point(207, 70);
            this.lbl_port.Name = "lbl_port";
            this.lbl_port.Size = new System.Drawing.Size(37, 13);
            this.lbl_port.TabIndex = 8;
            this.lbl_port.Text = "PORT";
            // 
            // gb_connection
            // 
            this.gb_connection.Controls.Add(this.rb_ethernet);
            this.gb_connection.Controls.Add(this.rb_usb);
            this.gb_connection.Location = new System.Drawing.Point(115, 328);
            this.gb_connection.Name = "gb_connection";
            this.gb_connection.Size = new System.Drawing.Size(307, 49);
            this.gb_connection.TabIndex = 9;
            this.gb_connection.TabStop = false;
            this.gb_connection.Text = "Connection Options";
            // 
            // rb_ethernet
            // 
            this.rb_ethernet.AutoSize = true;
            this.rb_ethernet.Checked = true;
            this.rb_ethernet.Location = new System.Drawing.Point(167, 19);
            this.rb_ethernet.Name = "rb_ethernet";
            this.rb_ethernet.Size = new System.Drawing.Size(122, 17);
            this.rb_ethernet.TabIndex = 1;
            this.rb_ethernet.TabStop = true;
            this.rb_ethernet.Text = "Ethernet Connection";
            this.rb_ethernet.UseVisualStyleBackColor = true;
            this.rb_ethernet.CheckedChanged += new System.EventHandler(this.rb_ethernet_CheckedChanged);
            // 
            // rb_usb
            // 
            this.rb_usb.AutoSize = true;
            this.rb_usb.Location = new System.Drawing.Point(33, 19);
            this.rb_usb.Name = "rb_usb";
            this.rb_usb.Size = new System.Drawing.Size(104, 17);
            this.rb_usb.TabIndex = 0;
            this.rb_usb.Text = "USB Connection";
            this.rb_usb.UseVisualStyleBackColor = true;
            this.rb_usb.CheckedChanged += new System.EventHandler(this.rb_usb_CheckedChanged);
            // 
            // tb_filename
            // 
            this.tb_filename.Location = new System.Drawing.Point(115, 287);
            this.tb_filename.Name = "tb_filename";
            this.tb_filename.Size = new System.Drawing.Size(228, 20);
            this.tb_filename.TabIndex = 11;
            this.tb_filename.Text = "File_Name";
            // 
            // lb_filename
            // 
            this.lb_filename.AutoSize = true;
            this.lb_filename.Location = new System.Drawing.Point(349, 294);
            this.lb_filename.Name = "lb_filename";
            this.lb_filename.Size = new System.Drawing.Size(63, 13);
            this.lb_filename.TabIndex = 12;
            this.lb_filename.Text = "FILE NAME";
            // 
            // gb_gps
            // 
            this.gb_gps.Controls.Add(this.tb_ip);
            this.gb_gps.Controls.Add(this.lbl_Address);
            this.gb_gps.Controls.Add(this.tb_port);
            this.gb_gps.Controls.Add(this.lbl_port);
            this.gb_gps.Location = new System.Drawing.Point(115, 176);
            this.gb_gps.Name = "gb_gps";
            this.gb_gps.Size = new System.Drawing.Size(307, 96);
            this.gb_gps.TabIndex = 13;
            this.gb_gps.TabStop = false;
            this.gb_gps.Text = "Trimble IP Settings";
            // 
            // gb_profile
            // 
            this.gb_profile.Controls.Add(this.lb_profile_port);
            this.gb_profile.Controls.Add(this.tb_profile_port);
            this.gb_profile.Controls.Add(this.lb_profile_ip);
            this.gb_profile.Controls.Add(this.tb_profile_ip);
            this.gb_profile.Location = new System.Drawing.Point(115, 60);
            this.gb_profile.Name = "gb_profile";
            this.gb_profile.Size = new System.Drawing.Size(307, 95);
            this.gb_profile.TabIndex = 14;
            this.gb_profile.TabStop = false;
            this.gb_profile.Text = "Pofile IP Settings";
            // 
            // lb_profile_port
            // 
            this.lb_profile_port.AutoSize = true;
            this.lb_profile_port.Location = new System.Drawing.Point(203, 65);
            this.lb_profile_port.Name = "lb_profile_port";
            this.lb_profile_port.Size = new System.Drawing.Size(37, 13);
            this.lb_profile_port.TabIndex = 9;
            this.lb_profile_port.Text = "PORT";
            // 
            // tb_profile_port
            // 
            this.tb_profile_port.Location = new System.Drawing.Point(46, 62);
            this.tb_profile_port.Name = "tb_profile_port";
            this.tb_profile_port.Size = new System.Drawing.Size(151, 20);
            this.tb_profile_port.TabIndex = 10;
            this.tb_profile_port.Text = "24691";
            // 
            // lb_profile_ip
            // 
            this.lb_profile_ip.AutoSize = true;
            this.lb_profile_ip.Location = new System.Drawing.Point(203, 26);
            this.lb_profile_ip.Name = "lb_profile_ip";
            this.lb_profile_ip.Size = new System.Drawing.Size(72, 13);
            this.lb_profile_ip.TabIndex = 9;
            this.lb_profile_ip.Text = "IP ADDRESS";
            // 
            // tb_profile_ip
            // 
            this.tb_profile_ip.Location = new System.Drawing.Point(46, 23);
            this.tb_profile_ip.Name = "tb_profile_ip";
            this.tb_profile_ip.Size = new System.Drawing.Size(151, 20);
            this.tb_profile_ip.TabIndex = 0;
            this.tb_profile_ip.Text = "192.168.2.92";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 462);
            this.Controls.Add(this.gb_profile);
            this.Controls.Add(this.gb_gps);
            this.Controls.Add(this.lb_filename);
            this.Controls.Add(this.tb_filename);
            this.Controls.Add(this.gb_connection);
            this.Controls.Add(this.btn_stop);
            this.Controls.Add(this.lbl_title);
            this.Controls.Add(this.btn_start);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Data Logging Program";
            this.gb_connection.ResumeLayout(false);
            this.gb_connection.PerformLayout();
            this.gb_gps.ResumeLayout(false);
            this.gb_gps.PerformLayout();
            this.gb_profile.ResumeLayout(false);
            this.gb_profile.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.Button btn_stop;
        private System.Windows.Forms.TextBox tb_ip;
        private System.Windows.Forms.Label lbl_Address;
        private System.Windows.Forms.TextBox tb_port;
        private System.Windows.Forms.Label lbl_port;
        private System.Windows.Forms.GroupBox gb_connection;
        private System.Windows.Forms.RadioButton rb_ethernet;
        private System.Windows.Forms.RadioButton rb_usb;
        private System.Windows.Forms.TextBox tb_filename;
        private System.Windows.Forms.Label lb_filename;
        private System.Windows.Forms.GroupBox gb_gps;
        private System.Windows.Forms.GroupBox gb_profile;
        private System.Windows.Forms.Label lb_profile_port;
        private System.Windows.Forms.TextBox tb_profile_port;
        private System.Windows.Forms.Label lb_profile_ip;
        private System.Windows.Forms.TextBox tb_profile_ip;
    }
}

