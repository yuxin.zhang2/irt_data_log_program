﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace DataLogger
{
    public class LoggingThread
    {
        private TCP_Client _tcp_client;
        private MainProgram _p;
        private string _filename;
        private static int _buffer_size = 1;
        private ProfileData[] _pData_buffer_1 = new ProfileData[_buffer_size];
        private ProfileData[] _pData_buffer_2 = new ProfileData[_buffer_size];
        private string[] _gps_info_buffer_1 = new string[_buffer_size];
        private string[] _gps_info_buffer_2 = new string[_buffer_size];
        private string[] _time_buffer_1 = new string[_buffer_size];
        private string[] _time_buffer_2 = new string[_buffer_size];
        private int _counter = 0;
        private bool _isBuffer1 = true;
        private StreamWriter _sw;
        Thread saving_Thread_1;
        Thread saving_Thread_2;
        //Thread saving_Thread_3;
        //Thread saving_Thread_4;

        public LoggingThread(StreamWriter sw, TCP_Client tcp_client, MainProgram p_v, string filename_v)
        {
            _tcp_client = tcp_client;
            _p = p_v;
            _filename = filename_v;
            _sw = sw;
        }

        public void startLogging()
        {
            try
            {
                _tcp_client.StartConn();

                while (true)
                {
                    _pData_buffer_1[_counter] = _p.GetProfile();
                    _gps_info_buffer_1[_counter] = _tcp_client.getCurrentInfo();
                    _time_buffer_1[_counter] = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.FFF");
                    bool status = _p.ExportToProfile(_sw, _pData_buffer_1, _gps_info_buffer_1, _time_buffer_1, _filename);
                    System.Console.WriteLine("Saving Status:" + status);
                    /*
                    if ((_counter < _buffer_size) && _isBuffer1)
                    {
                        _pData_buffer_1[_counter] = _p.GetProfile();
                        _gps_info_buffer_1[_counter] = _tcp_client.getCurrentInfo();
                        _time_buffer_1[_counter] = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.FFF");
                        _counter++;
                        System.Console.WriteLine("Buffer 1 size:" + _counter);
                        continue;
                    }
                    else if ((_counter == _buffer_size) && _isBuffer1)
                    {

                        _counter = 0;
                        FlieSavingThread ft = new FlieSavingThread(_sw, _p, _pData_buffer_1, _gps_info_buffer_1, _time_buffer_1, _filename);
                        saving_Thread_1 = new Thread(new ThreadStart(ft.startSaving));
                        saving_Thread_1.IsBackground = true;
                        saving_Thread_1.Start();
                        Array.Clear(_pData_buffer_1, 0, _buffer_size);
                        Array.Clear(_gps_info_buffer_1, 0, _buffer_size);
                        Array.Clear(_time_buffer_1, 0, _buffer_size);
                        _isBuffer1 = false;
                        
                    }
                    else if ((_counter < _buffer_size) && !_isBuffer1)
                    {
                        _pData_buffer_2[_counter] = _p.GetProfile();
                        _gps_info_buffer_2[_counter] = _tcp_client.getCurrentInfo();
                        _time_buffer_2[_counter] = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.FFF");
                        _counter++;
                        System.Console.WriteLine("Buffer 2 size:" + _counter);
                        continue;
                    }
                    else if ((_counter == _buffer_size) && !_isBuffer1)
                    {

                        _counter = 0;
                        FlieSavingThread ft_2 = new FlieSavingThread(_sw, _p, _pData_buffer_2, _gps_info_buffer_2, _time_buffer_2, _filename);
                        saving_Thread_2 = new Thread(new ThreadStart(ft_2.startSaving));
                        saving_Thread_2.IsBackground = true;
                        saving_Thread_2.Start();
                        Array.Clear(_pData_buffer_2, 0, _buffer_size);
                        Array.Clear(_gps_info_buffer_2, 0, _buffer_size);
                        Array.Clear(_time_buffer_2, 0, _buffer_size);
                        _isBuffer1 = true;
                        /*
                        _counter = 0;
                        bool status = _p.ExportToProfile(_sw, _pData_buffer_1, _gps_info_buffer_1, _time_buffer_1, _filename);
                        Array.Clear(_pData_buffer_1, 0, _buffer_size);
                        Array.Clear(_gps_info_buffer_1, 0, _buffer_size);
                        Array.Clear(_time_buffer_1, 0, _buffer_size);
                        System.Console.WriteLine("Saving Status:" + status);
                        
                    }
                    
                    if ((_counter < _buffer_size))
                    {
                        _pData_buffer_1[_counter] = _p.GetProfile();
                        _gps_info_buffer_1[_counter] = _tcp_client.getCurrentInfo();
                        _time_buffer_1[_counter] = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.FFF");
                        _counter++;
                        System.Console.WriteLine("Buffer 1 size:" + _counter);
                        continue;
                    }
                    else
                    {
                        _counter = 0;
                        bool status = _p.ExportToProfile(_sw, _pData_buffer_1, _gps_info_buffer_1, _time_buffer_1, _filename);
                        Array.Clear(_pData_buffer_1, 0, _buffer_size);
                        Array.Clear(_gps_info_buffer_1, 0, _buffer_size);
                        Array.Clear(_time_buffer_1, 0, _buffer_size);
                        System.Console.WriteLine("Saving Status:" + status);
                    }
                    */



                    //System.Console.WriteLine("Test");
                }
            }
            finally
            {
                Thread.Sleep(1000);
            }   
        }
    }
}
