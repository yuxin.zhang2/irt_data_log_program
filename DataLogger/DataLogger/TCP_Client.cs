﻿using SimpleTCP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLogger
{
    public class TCP_Client
    {
        private string _ip_addr;
        private int _port;
        private SimpleTcpClient _client;
        private string _GPSInfo;

        public TCP_Client(string ip_addr, int port)
        {
            _ip_addr = ip_addr;
            _port = port;
        }
        public void StartConn()
        {
            _client = new SimpleTcpClient().Connect(_ip_addr, _port);
            _client.StringEncoder = Encoding.UTF8;
            _client.DataReceived += Client_DataRecieved;
        }

        private void Client_DataRecieved(object sender, SimpleTCP.Message e)
        {
            _GPSInfo = e.MessageString;
        }

        public string getCurrentInfo()
        {
            return _GPSInfo;
        }
        public void EndConn()
        {
            _client.Disconnect();
        }
    }
}
