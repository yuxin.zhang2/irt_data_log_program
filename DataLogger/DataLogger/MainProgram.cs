﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Threading;

namespace DataLogger
{
    public class MainProgram
    {
        #region Constants

        /// <summary>
        /// Measurement range X direction
        /// </summary>
        public const int MEASURE_RANGE_FULL = 800;
        public const int MEASURE_RANGE_MIDDLE = 600;
        public const int MEASURE_RANGE_SMALL = 400;
        #endregion

        #region Enum

        /// <summary>
        /// Send command definition
        /// </summary>
        /// <remark>Defined for separate return code distinction</remark>
        public enum SendCommand
        {
            /// <summary>None</summary>
            None,
            /// <summary>Restart</summary>
            RebootController,
            /// <summary>Trigger</summary>
            Trigger,
            /// <summary>Start measurement</summary>
            StartMeasure,
            /// <summary>Stop measurement</summary>
            StopMeasure,
            /// <summary>Auto zero</summary>
            AutoZero,
            /// <summary>Timing</summary>
            Timing,
            /// <summary>Reset</summary>
            Reset,
            /// <summary>Program switch</summary>
            ChangeActiveProgram,
            /// <summary>Get measurement results</summary>
            GetMeasurementValue,

            /// <summary>Get profiles</summary>
            GetProfile,
            /// <summary>Get batch profiles (operation mode "high-speed (profile only)")</summary>
            GetBatchProfile,
            /// <summary>Get profiles (operation mode "advanced (with OUT measurement)")</summary>
            GetProfileAdvance,
            /// <summary>Get batch profiles (operation mode "advanced (with OUT measurement)").</summary>
            GetBatchProfileAdvance,

            /// <summary>Start storage</summary>
            StartStorage,
            /// <summary>Stop storage</summary>
            StopStorage,
            /// <summary>Get storage status</summary>
            GetStorageStatus,
            /// <summary>Manual storage request</summary>
            RequestStorage,
            /// <summary>Get storage data</summary>
            GetStorageData,
            /// <summary>Get profile storage data</summary>
            GetStorageProfile,
            /// <summary>Get batch profile storage data.</summary>
            GetStorageBatchProfile,

            /// <summary>Initialize USB high-speed data communication</summary>
            HighSpeedDataUsbCommunicationInitalize,
            /// <summary>Initialize Ethernet high-speed data communication</summary>
            HighSpeedDataEthernetCommunicationInitalize,
            /// <summary>Request preparation before starting high-speed data communication</summary>
            PreStartHighSpeedDataCommunication,
            /// <summary>Start high-speed data communication</summary>
            StartHighSpeedDataCommunication,
        }

        #endregion

        #region Field

        /// <summary>Ethernet settings structure </summary>
        private LJV7IF_ETHERNET_CONFIG _ethernetConfig;
        /// <summary>Measurement data list</summary>
        //private List<MeasureData> _measureDatas;
        /// <summary>Current device ID</summary>
        private int _currentDeviceId;
        /// <summary>Send command</summary>
        private SendCommand _sendCommand;
        /// <summary>Callback function used during high-speed communication</summary>
        private HighSpeedDataCallBack _callback;
        /// <summary>Callback function used during high-speed communication (count only)</summary>
        private HighSpeedDataCallBack _callbackOnlyCount;

        /// The following are maintained in arrays to support multiple controllers.
        /// <summary>Array of profile information structures</summary>
        private LJV7IF_PROFILE_INFO[] _profileInfo;
        /// <summary>Array of controller information</summary>
        private DeviceData[] _deviceData;
        /// <summary>Array of labels that indicate the controller status</summary>
        private String[] _deviceStatusLabels;
        /// <summary>Array of labels that indicate the number of received profiles </summary>
        private String[] _receivedProfileCountLabels;

        #endregion
        public void Initialize()
        {
            int rc = NativeMethods.LJV7IF_Initialize();

            for (int i = 0; i < _deviceData.Length; i++)
            {
                _deviceData[i].Status = DeviceStatus.NoConnection;
                _deviceStatusLabels[i] = _deviceData[i].GetStatusString();
            }
        }

        #region Button for establishing and disconnecting the communication path with the controller
        /// <summary>
        /// "UsbOpen" button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void UsbOpen()
        {
            int rc = NativeMethods.LJV7IF_UsbOpen(_currentDeviceId);
            // @Point
            // # Enter the "_currentDeviceId" set here for the communication settings into the arguments of each DLL function.
            // # If you want to get data from multiple controllers, prepare and set multiple "_currentDeviceId" values,
            //   enter these values into the arguments of the DLL functions, and then use the functions.

            _deviceData[_currentDeviceId].Status = (rc == (int)Rc.Ok) ? DeviceStatus.Usb : DeviceStatus.NoConnection;
            _deviceStatusLabels[_currentDeviceId] = _deviceData[_currentDeviceId].GetStatusString();
        }

        /// <summary>
        /// "EthernetOpen" button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void EthernetOpen(string ip, string port)
        {
            string[] ip_segments = ip.Split('.'); 

            LJV7IF_ETHERNET_CONFIG ethernetConfig = new LJV7IF_ETHERNET_CONFIG();
            try
            {
                ethernetConfig.abyIpAddress = new byte[]
                {
                        Convert.ToByte(ip_segments[0]),
                        Convert.ToByte(ip_segments[1]),
                        Convert.ToByte(ip_segments[2]),
                        Convert.ToByte(ip_segments[3])
                };
                ethernetConfig.wPortNo = Convert.ToUInt16(port);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                return;
            }

            // @Point
            // # Enter the "_currentDeviceId" set here for the communication settings into the arguments of each DLL function.
            // # If you want to get data from multiple controllers, prepare and set multiple "_currentDeviceId" values,
            //   enter these values into the arguments of the DLL functions, and then use the functions.

            int rc = NativeMethods.LJV7IF_EthernetOpen(_currentDeviceId, ref ethernetConfig);

            if (rc == (int)Rc.Ok)
            {
                _deviceData[_currentDeviceId].Status = DeviceStatus.Ethernet;
                _deviceData[_currentDeviceId].EthernetConfig = ethernetConfig;
            }
            else
            {
                _deviceData[_currentDeviceId].Status = DeviceStatus.NoConnection;
            }
            _deviceStatusLabels[_currentDeviceId] = _deviceData[_currentDeviceId].GetStatusString();

        }

        /// <summary>
        /// "CommClose" button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void CommClose()
        {
            int rc = NativeMethods.LJV7IF_CommClose(_currentDeviceId);

            _deviceData[_currentDeviceId].Status = DeviceStatus.NoConnection;
            _deviceStatusLabels[_currentDeviceId] = _deviceData[_currentDeviceId].GetStatusString();
        }
        #endregion


        #region Button for getting the measurement results
        /// <summary>
        /// Get the profile data size
        /// </summary>
        /// <returns>Data size of one profile (in units of bytes)</returns>
        private uint GetOneProfileDataSize()
        {
            // Buffer size (in units of bytes)
            uint retBuffSize = 0;

            // Basic size
            int basicSize = MEASURE_RANGE_FULL;// (int)_cmbMeasureX.SelectedValue / (int)_cmbReceivedBinning.SelectedValue;
            //basicSize /= (int)_cmbCompressX.SelectedValue;

            // Number of headers
            //retBuffSize += (uint)basicSize * (_rdbtnOneHead.Checked ? 1U : 2U);
            retBuffSize += (uint)basicSize * 2U;

            // Envelope setting
            //retBuffSize *= (_chkboxEnvelope.Checked ? 2U : 1U);

            //in units of bytes
            retBuffSize *= (uint)sizeof(uint);// Marshal.SizeOf(typeof(uint));

            // Sizes of the header and footer structures
            LJV7IF_PROFILE_HEADER profileHeader = new LJV7IF_PROFILE_HEADER();
            retBuffSize += (uint)Marshal.SizeOf(profileHeader);
            LJV7IF_PROFILE_FOOTER profileFooter = new LJV7IF_PROFILE_FOOTER();
            retBuffSize += (uint)Marshal.SizeOf(profileFooter);

            return retBuffSize;
        }

        /// <summary>
		/// AnalyzeProfileData
		/// </summary>
		/// <param name="profileCount">Number of profiles that were read</param>
		/// <param name="profileInfo">Profile information structure</param>
		/// <param name="profileData">Acquired profile data</param>
		/// <param name="startProfileIndex">Start position of the profiles to copy</param>
		/// <param name="dataUnit">Profile data size</param>
		private void AnalyzeProfileData(int profileCount, ref LJV7IF_PROFILE_INFO profileInfo, int[] profileData, int startProfileIndex, int dataUnit)
        {
            int readPropfileDataSize = ProfileData.CalculateDataSize(profileInfo);
            int[] tempRecvieProfileData = new int[readPropfileDataSize];

            // Profile data retention
            for (int i = 0; i < profileCount; i++)
            {
                Array.Copy(profileData, (startProfileIndex + i * dataUnit), tempRecvieProfileData, 0, readPropfileDataSize);

                _deviceData[_currentDeviceId].ProfileData.Add(new ProfileData(tempRecvieProfileData, profileInfo));
            }
        }

        /// <summary>
        /// AnalyzeProfileData
        /// </summary>
        /// <param name="profileCount">Number of profiles that were read</param>
        /// <param name="profileInfo">Profile information structure</param>
        /// <param name="profileData">Acquired profile data</param>
        private void AnalyzeProfileData(int profileCount, ref LJV7IF_PROFILE_INFO profileInfo, int[] profileData)
        {
            int dataUnit = ProfileData.CalculateDataSize(profileInfo);
            AnalyzeProfileData(profileCount, ref profileInfo, profileData, 0, dataUnit);
        }

        /// <summary>
		/// Profile output
		/// </summary>
		/// <param name="datas">Profile data</param>
		/// <param name="profileNo">Profile information</param>
		/// <param name="fileName">File name</param>
		/// <returns></returns>
        public bool ExportToProfile(System.IO.StreamWriter sw, ProfileData[] datas, string[] gps_strs, string[] time_strs, string fileName)
        {
            try
            {
                var Timestamp_file_save_start = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();
                int len = time_strs.Length;
                System.Console.WriteLine("file length:" + len);
                for (int i = 0; i < len; i++)
                {
                    sw.WriteLine(time_strs[i]);
                    sw.WriteLine("GPS Info:");
                    sw.WriteLine(gps_strs[i]);
                    sw.WriteLine("Profile Data:");
                    sw.WriteLine(datas[i].ToString());
                }
                //using (sw)
                //{
                        
                    //try
                    //{
                    //    int len = time_strs.Length;
                    //    System.Console.WriteLine("file length:" + len);
                    //    for (int i = 0; i < len; i++)
                    //    {
                    //        sw.WriteLine(time_strs[i]);
                    //        sw.WriteLine("GPS Info:");
                    //        sw.WriteLine(gps_strs[i]);
                    //        sw.WriteLine("Profile Data:");
                    //        sw.WriteLine(datas[i].ToString());
                    //        System.Console.WriteLine("file save:" + i + "th");
                    //    }
                    //}
                    //finally
                    //{
                    //    //sw.Dispose();
                    //    sw.Close();
                    //}
                //}
                var Timestamp_file_save_end = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();
                int diff = Convert.ToInt32(Timestamp_file_save_end - Timestamp_file_save_start);
                Thread.Sleep(1);
                System.Console.WriteLine("file save time:"+diff);
            }
            catch (Exception ex)
            {
                // File save failure
                //System.Diagnostics.Debug.WriteLine(ex.Message);
                //System.Diagnostics.Debug.Assert(false);
                return false;
            }
            return true;
        }
        /*
		static public bool ExportOneProfile(ProfileData[] datas, int profileNo, string fileName, string gps_str)
        {
            try
            {
                var Timestamp_file_save_start = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();
                Encoding unicode = System.Text.Encoding.GetEncoding("utf-8");
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(fileName, append:true, encoding:unicode))
                {
                    try
                    {
                        if (datas[0] == null) return false;
                        string date_time_str = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.FFF");
                        sw.WriteLine(date_time_str);
                        sw.WriteLine("GPS Info:");
                        sw.WriteLine(gps_str);
                        sw.WriteLine("Profile Data:");
                        sw.WriteLine(datas[profileNo].ToString());
                    }
                    finally
                    {
                        sw.Close();
                    }
                }
                var Timestamp_file_save_end = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();
                System.Console.WriteLine("file save time:");
                System.Console.WriteLine(Timestamp_file_save_end-Timestamp_file_save_start);
            }
            catch (Exception ex)
            {
                // File save failure
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.Assert(false);
                return false;
            }

            return true;
        }
        */

        /// <summary>
        /// "GetProfile" button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public ProfileData GetProfile()
        {
            ProfileData r_value;
            _sendCommand = SendCommand.GetProfile;

            _deviceData[_currentDeviceId].ProfileData.Clear();
            LJV7IF_GET_PROFILE_REQ _req = new LJV7IF_GET_PROFILE_REQ();
            try
            {
                _req.byTargetBank = Convert.ToByte("00", 16);//00: Active Surface
                _req.byPosMode = Convert.ToByte("00", 16);//00: From Current Profile Position
                _req.dwGetProfNo = Convert.ToUInt16("0");//0: Only applies when dwGetProfNo is 0x02 (Specific Profile)
                _req.byGetProfCnt = Convert.ToByte("1");//Number of Profiles to get acquisition of
                _req.byErase = Convert.ToByte("0");//0: Donot erase profiles that were read
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                return null;
            }


            LJV7IF_GET_PROFILE_RSP rsp = new LJV7IF_GET_PROFILE_RSP();
            LJV7IF_PROFILE_INFO profileInfo = new LJV7IF_PROFILE_INFO();
            uint oneProfDataBuffSize = GetOneProfileDataSize();
            uint allProfDataBuffSize = oneProfDataBuffSize * _req.byGetProfCnt;
            int[] profileData = new int[allProfDataBuffSize / Marshal.SizeOf(typeof(int))];


            /*using (PinnedObject pin = new PinnedObject(profileData))
            {
                int rc = NativeMethods.LJV7IF_GetProfile(_currentDeviceId, ref _req, ref rsp, ref profileInfo, pin.Pointer, allProfDataBuffSize);                
            }*/

            // Changing to "profileData" Unmanaged code so the CLR/.NET garbage collector doesn't touch it
            // Pin the "profileData" to protect it from the garbage collector.
            GCHandle _Handle = GCHandle.Alloc(profileData, GCHandleType.Pinned);
            int rc = NativeMethods.LJV7IF_GetProfile(_currentDeviceId, ref _req, ref rsp, ref profileInfo, _Handle.AddrOfPinnedObject(), allProfDataBuffSize);
            if (rc == (int)Rc.Ok)
            {
                // Response data display
                //AddLog(Utility.ConvertToLogString(rsp).ToString());
                //AddLog(Utility.ConvertToLogString(profileInfo).ToString());

                AnalyzeProfileData((int)rsp.byGetProfCnt, ref profileInfo, profileData);

                // Profile export
                //if (ExportOneProfile(_deviceData[_currentDeviceId].ProfileData.ToArray(), 0, filename, gps_str))
                //{
                //    System.Console.WriteLine(@"###Saved!!");
                //}
                r_value = _deviceData[_currentDeviceId].ProfileData.ToArray()[0];
                // Free the unmanaged allocated memory for "profileData"
                if (_Handle.IsAllocated) _Handle.Free();
                return r_value;
            }
            else
            {
                // Free the unmanaged allocated memory for "profileData"
                if (_Handle.IsAllocated) _Handle.Free();
                return null;
            }
        }
        #endregion

        public MainProgram()
        {
            // Field initialization
            _sendCommand = SendCommand.None;
            _deviceData = new DeviceData[NativeMethods.DeviceCount];
            _deviceStatusLabels = new String[NativeMethods.DeviceCount];

            for (int i = 0; i < NativeMethods.DeviceCount; i++)
            {
                _deviceData[i] = new DeviceData();
                _deviceStatusLabels[i] = _deviceData[i].GetStatusString();
            }

            // Set Device Id to be fixed 0 // Can be used in future for multiple controller acquisitions
            _currentDeviceId = 0;
        }
    }
}
